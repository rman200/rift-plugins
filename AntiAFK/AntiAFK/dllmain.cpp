#include "stdafx.h"
#include "Common.h"
#include "sdkapi.h"
#include <random>

PSDK_CONTEXT SDK_CONTEXT_GLOBAL;

bool b_AntiAFK;
namespace AntiAFK {
	constexpr int WarningTime = 100;
	float lastAction;

	std::mt19937 rng;
	std::uniform_int_distribution<std::mt19937::result_type> randTime(-45, +45);
	int rValue = 0;

	void ResetSeed() {
		rng.seed(std::random_device()());
	}

	void ResetActionTimer() {
		lastAction = GameTime();
	}

	void Execute() {
		if (GameTime() - lastAction >= WarningTime + rValue) {
			rValue = (int)randTime(rng);
			SdkUiConsoleWrite(" [AntiAFK] Moving To Avoid Inactivity. TimePassed: %f Next Move In: %d.\n", GameTime() - lastAction, WarningTime + rValue);
			MoveTo(GetPosition(LocalPlayer()));
			lastAction = GameTime();

		}
	}
};

void __cdecl DrawOverlayScene(_In_ void* UserData)
{
	UNREFERENCED_PARAMETER(UserData);
	SdkUiCheckbox("AntiAFK", &b_AntiAFK, NULL);
}

void __cdecl AntiAFKTick(void* UserData) {
	UNREFERENCED_PARAMETER(UserData);

	if (b_AntiAFK) {
		AntiAFK::Execute();
	}
}

BOOL
WINAPI
DllMain(
	_In_ HINSTANCE hinstDLL,
	_In_ DWORD fdwReason,
	_In_ LPVOID lpvReserved
)

{
	UNREFERENCED_PARAMETER(hinstDLL);

	if (fdwReason != DLL_PROCESS_ATTACH)
		return TRUE;

	SDK_EXTRACT_CONTEXT(lpvReserved);
	if (!SDK_CONTEXT_GLOBAL || !SDKSTATUS_SUCCESS(SdkNotifyLoadedModule("AntiAFK", SDK_VERSION)))
		return FALSE;

	SdkRegisterOverlayScene(DrawOverlayScene, NULL);

	AntiAFK::ResetSeed();
	SdkRegisterGameScene(AntiAFKTick, NULL);

	//TODO: ResetActionTimer() on IssueOrder

	return TRUE;
}