#pragma once

#include "sdkapi.h"
extern PSDK_CONTEXT SDK_CONTEXT_GLOBAL;

void MoveTo(SDKVECTOR Pos, bool Pet = false) {
	if (!SDKSTATUS_SUCCESS(SdkMoveLocalPlayer(&Pos, Pet))) {
		SdkUiConsoleWrite("[SDK] Error! Could not move to pos.\n");
	}
}

#define MAKE_COMMON(NAME,TYPE,FUNCTIONDEF)                    \
TYPE NAME##(){                                        \
    TYPE _##NAME;                                        \
    SDKSTATUS _STATE = FUNCTIONDEF(&_##NAME);    \
    if (!SDKSTATUS_SUCCESS(_STATE)) {                    \
        SdkUiConsoleWrite(" [SDK] Error! MAKE_COMMON Could not retrieve %s errorCode: %i.\n", __FUNCTION__, (int)_STATE); \
    }                                                    \
    return _##NAME;                                     \
}

MAKE_COMMON(LocalPlayer, void*, SdkGetLocalPlayer);
MAKE_COMMON(GameTime, float, SdkGetGameTime);

#define MAKE_GET(NAME,TYPE,FUNCTIONDEF)                    \
TYPE Get##NAME##(void* Object){                                        \
    TYPE _##NAME;                                        \
    SDKSTATUS _STATE = FUNCTIONDEF(Object,&_##NAME);    \
    if (!SDKSTATUS_SUCCESS(_STATE)) {                    \
        SdkUiConsoleWrite(" [SDK] Error! MAKE_GET Could not retrieve %s for obj %p errorCode: %i.\n", __FUNCTION__, Object, (int)_STATE); \
    }                                                    \
    return _##NAME;                                     \
}
MAKE_GET(Position, SDKVECTOR, SdkGetObjectPosition);

